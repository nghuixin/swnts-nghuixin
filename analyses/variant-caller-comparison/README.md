# Variant Caller Comparison

Work in Progress


This analysis compares the output of different variant callers and is guided in part by 
the [guidelines for evaluating germline variant calls published by GATK](https://gatk.broadinstitute.org/hc/en-us/articles/360035531572-Evaluating-the-quality-of-a-germline-short-variant-callset)

Analysis is currently only being done on germline variants found in WES of blood samples. 

### Running
The analysis can be run using ./bash run-new-analysis.sh and can be done using
the included Dockerfile

### Known issues
01-preprocess-data.sh may generate warnings when using vcftools. These are warnings
documented repeatedly on the vcftools github page saying that their tool cannot
parse vcf headers that have commas within the "description" sections. This has
no effect on output, but could be solved by removing those commas.

03-germline-comparison-plots.R is not using the Lato font descibed as part of the
Sage theme. Lato is not installed in the current Docker. This generates a warning
and causes plots to switch to default ggplot2 fonts

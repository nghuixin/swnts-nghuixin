# This for loop iterates over each file that matches the pattern swn_patient_*_blood.vcf.vcf.gz in the current directory
for file in swn_patient_*_blood.vcf.vcf.gz; do

    # Decompress the current VCF file with zcat, then pipe the output into sed
    zcat "$file" | \
    # Use sed to perform several substitution operations on the lines starting with ##INFO=<ID=
    sed -e '/^##INFO=<ID=/ s/Description=\\\"Description=/Description="/g' \  # Replace 'Description=\\\"Description=' with 'Description="' to start the description field
        -e '/^##INFO=<ID=/ s/\\\">/"/g' \  # Replace the incorrect '\\\">' at the end of the INFO line with a single quote
        -e '/^##INFO=<ID=/ s/\"\\\">/"/g' \  # Replace '\"\\\">' with '"' in case there is an additional quote
        -e '/^##INFO=<ID=/ s/\\"//g' |  # Remove all remaining escaped double quotes within the INFO field
    bgzip > "${file%.vcf.vcf.gz}_corrected.vcf.gz"  # Compress the output with bgzip and rename the file to indicate it has been corrected

    # Index the newly corrected and compressed VCF file with tabix
    tabix -p vcf "${file%.vcf.gz}_corrected.vcf.gz"
done
# The % in ${file%.vcf.vcf.gz} is a parameter expansion in bash that strips the shortest match of '.vcf.vcf.gz' from the end of $file

#!/bin/bash

# Define the input directory where your VCF files are located
input_dir="./*.gz"

# Define the output directory where you want to store the processed files
output_dir="../fixed_header"

# Iterate over all VCF files in the input directory
for vcf_file in $input_dir; do

    # Modify the original VCF file using AWK
    zcat "$vcf_file" | awk '{if($0~/^##/ && $0~/Description/){gsub(/"/,"",$0); split($0,descr,"Description="); print descr[1]"Description=\""substr(descr[2],1,length(descr[2])-2)"\">"} else{print $0}}' > "$output_dir/fheader_$(basename "$vcf_file" .vcf.gz).vcf"

        # Create a gz version of the header file and tabix it
    bgzip -c "$output_dir/fheader_$(basename "$vcf_file" .vcf.gz).vcf" > "$output_dir/fheader_$(basename "$vcf_file" .vcf.gz).vcf.gz"
    tabix -p vcf "$output_dir/fheader_$(basename "$vcf_file" .vcf.gz).vcf.gz"
done

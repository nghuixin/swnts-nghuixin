## Scripts exclusively for publication-ready plots

We list information about the relevant figures and modules for each script in this directory below for convenience.

| Individual script | Figure(s) | Linked analysis modules |
|--------|--------|------------------|
